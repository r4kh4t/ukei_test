import React from "react";
import { calculateWinner } from "utils";
import Board from "components/Game/Board";

const Game = () => {
  const [history, setHistory] = React.useState([
    { squares: Array(9).fill(null) },
  ]);
  const [stepNumber, setStepNumber] = React.useState(0);
  const [xIsNext, setIsNext] = React.useState(!Math.round(Math.random()));
  const current = history[stepNumber];
  const winner = calculateWinner(current.squares);

  const handleClick = (i) => {
    const historyTemp = history.slice(0, stepNumber + 1);
    const current = historyTemp[historyTemp.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = xIsNext ? "X" : "O";
    setHistory(historyTemp.concat([{ squares: squares }]));
    setStepNumber(historyTemp.length);
    setIsNext(!xIsNext);
  };

  const handleReset = () => {
    setHistory([{ squares: Array(9).fill(null) }]);
    setStepNumber(0);
    setIsNext(!Math.round(Math.random()));
  };

  let status;
  if (winner) {
    status = "Winner: " + winner;
    alert(`Winner is: ${winner}`);
  } else {
    status = "Current player: " + (xIsNext ? "X" : "O");
  }

  return (
    <>
      <div className="header-text">Welcome to the Tic Tac Toe</div>
      <div className="game">
        <Board squares={current.squares} onClick={(i) => handleClick(i)} />
        <div className="game-info">
          <div>{status}</div>
          <button onClick={handleReset}>Reset</button>
        </div>
      </div>
    </>
  );
};

export default Game;
