import React from "react";
import { Link } from "react-router-dom";
import { ROUTES } from "utils";

const Home = () => {
  return (
    <Link to={ROUTES.GAME} className="home">
      Welcome to the Tic Tac Toe
    </Link>
  );
};

export default Home;
