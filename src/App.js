import { BrowserRouter, Switch, Route } from "react-router-dom";
import React from "react";
import { ROUTES } from "utils";

import Home from "components/Home";
import Game from "components/Game";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path={ROUTES.GAME} component={Game} />
        <Route path={ROUTES.HOME} component={Home} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
